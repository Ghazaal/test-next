/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{tsx,ts,js,jsx}",
    "./src/components/**/*.{tsx,ts,js,jsx}",
    "./src/pages/**/*.{tsx,ts,js,jsx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}
