import React from 'react'
import Head from 'next/head'
import Header from './Header'
import Footer from './Footer'

interface LayoutProps {
  title: string
  children: JSX.Element
}

const Layout = ({ title, children }: LayoutProps) => {
  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge" />
        <title>{title ? title : ''}</title>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=5.0, minimum-scale=1.0"
        />
        <link rel="shortcut icon" href="/gameriaLogo.ico" />
        <meta property="og:title" content={title} key="ogtitle" />
        <meta property="og:type" content="website" />
      </Head>
      <main>
        <Header />
        {children}
        <Footer />
      </main>
    </>
  )
}

export default Layout
