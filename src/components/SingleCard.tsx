import Link from 'next/link'

interface PostTypes {
  body: string
  id: number
  title: string
  userId: number
}

const SingleCard = ({ data }: any) => {
  return (
    <div className="h-screen p-10">
      <Link href="/">
        <a>
          <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
            GO Back
          </button>
        </a>
      </Link>

      <p className="text-9xl font-bold text-center text-violet-900">
        {data.title}
      </p>
      <p className="mt-10 font-bold text-center text-2xl">{data.body}</p>
      <p className="mt-8 text-lg text-slate-500">{`Author : ${data.userId}`}</p>
    </div>
  )
}

export default SingleCard
