interface CardTypes {
    data: {
      userId: number
      id: number
      title: string
      body: string
    }
  }
  
  export default function Card({ data }: CardTypes) {
    return (
      <div className="h-36 rounded-lg shadow-xl p-4">
        <a href={`/post/${data.id}`}>
          <h5 className="text-violet-900 y text-center text-lg font-bold">
            {data.title}
          </h5>
          <p className="text-center h-46  truncate">{data.body}</p>
        </a>
      </div>
    )
  }
  