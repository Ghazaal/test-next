import Layout from '../../../components/layout'
import SingleCard from '../../../components/SingleCard'

interface PostTypes {
  post: {
    body: string
    id: number
    title: string
    userId: number
  }
}

const Post = (props: PostTypes) => {
  return (
    <Layout title={props.post.title}>
      <SingleCard data={props?.post} />
    </Layout>
  )
}

export default Post

export async function getServerSideProps(context: any) {
  const response = await fetch(
    `https://jsonplaceholder.typicode.com/posts/${context.query.id}`,
  )

  const post = await response.json()
  // Pass data to the page via props
  return { props: { post } }
}
