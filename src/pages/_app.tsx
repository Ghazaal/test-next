import React from "react"
import type { AppProps } from "next/app";
// import "../assets/styles/fonts.scss";
import "../assets/styles/global.scss";


function MyApp({ Component, pageProps }:AppProps) {
  return <Component {...pageProps} />
}

export default MyApp
