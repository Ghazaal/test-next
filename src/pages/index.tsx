import Layout from '../components/layout'
import Card from '../components/Card'

interface PostsTypes {
  posts: {
    userId: number
    id: number
    title: string
    body: string
  }[]
}

export default function Home(props: PostsTypes) {
  return (
    <Layout title="next test">
      <div className="grid grid-cols-4 gap-4 m-3">
        {props.posts?.map((post, index) => (
          <Card data={post} key={index} />
        ))}
      </div>
    </Layout>
  )
}

// This gets called on every request
export async function getServerSideProps() {
  const response = await fetch(
    'https://jsonplaceholder.typicode.com/posts?_limit=50',
  )

  const posts = await response.json()
  // Pass data to the page via props
  return { props: { posts } }
}
